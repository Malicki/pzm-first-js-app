(function(){
  function h(tag, props, children) {
    var el = document.createElement(tag);

    if(!!props) {
      var keys = Object.keys(props);
      keys.forEach(function(key){
        el[key] = props[key];
      })
    }

    if(!!children) {
      for(var i=0; i<children.length; i++) {
        var child = children[i];
        if(!!child) {
          if(typeof child == 'string') {
            child = document.createTextNode(child);
          }
          el.appendChild(child);
        }
      }
    }
    return el;
  }

  var state = {
    text: ''
  }


  var root = h('div',{},[
    h('label',{},['pole nr 1']),
    h('input',{
        className: 'small-text',
        type:'text', value:state.text,
        onchange: function(ev){
          state.text = ev.target.value;
        }
      }),
    h('button',{
      type:'button',
      onclick: function(){
        alert('hello '+state.text+'!');
      }
    },['kliknij mnie!'])
  ]);
  document.body.appendChild(root);

})()